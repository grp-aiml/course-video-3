# IIT KGP FOUNDATION OF AIML COURSE VIDEOS

## VIDEO SET 3

| SL# | TOPICS |
|:---:| :----- |
|  1  | Knowledge Representation Part 1 |
|  2  | Knowledge Representation Part 2 |
|  3  | Machine Learning-1 Part 1 |
|  4  | Machine Learning-1 Part 2 |
|  5  | Statistics and Data Analysis Part 1 |
|  6  | Statistics and Data Analysis Part 2 |